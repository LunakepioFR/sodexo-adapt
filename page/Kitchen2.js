import React, { useState} from 'react';
import { StyleSheet, Text, View, ImageBackground, TouchableOpacity, Image } from 'react-native';
import Header from '../components/header';
function Kitchen2({navigation}) {

    const ButtonTest = (props) => {
        const [isToggled, setToggle] = useState(false);

        const buttonPress = () => {
            setToggle(!isToggled);
        }

        if(isToggled) {
          return(
            <TouchableOpacity onPress={buttonPress} style={styles.buttonOn}>
                <View style={styles.icons}>
                  <Image source={require('../assets/carton.png')} style={styles.icon}/>
                  </View><View style={styles.right}><Text>{props.title}</Text></View>
            </TouchableOpacity>
           )
        }
        else{
          return(
            <TouchableOpacity onPress={buttonPress} style={styles.buttonOff}>
                <View style={styles.icons}>
                  <Image source={require('../assets/carton.png')} style={styles.icon}/>
                </View><View style={styles.right}><Text>{props.title}</Text></View>
            </TouchableOpacity>
           )
        }
     };
     

    const onPressHandler = () => {
      navigation.navigate('Home');
    }
  
    return(
        <View style={styles.container}>
        <ImageBackground source={require('../assets/fond13.jpg')} resizeMode="cover" style={styles.image}>
          <Header
              title='Découverte de la cuisine'
          />

        <View style={styles.row}>
        <ButtonTest title='Vestiaire'/>
        <ButtonTest title='Réception des marchandises'/>
        <ButtonTest title='Déconditionnement'/>
        </View>
        <View style={styles.row}>
        <ButtonTest title='Légumerie'/>
        <ButtonTest title='Stockage'/>
        <ButtonTest title='Préparation froide'/>
        </View>
        <View style={styles.row}>
        <ButtonTest title='Préparation chaude'/>
        <ButtonTest title='Conditionnement'/>
        <ButtonTest title='Distribution, relation client, salle à manger'/>
        </View>
        <View style={styles.row}>
        <ButtonTest title='Plonge vaisselle'/>
        <ButtonTest title='Plonge Batterie'/>
        <ButtonTest title='Local déchet'/>
        </View>
        <View style={styles.footer}>
          <TouchableOpacity onPress={onPressHandler}>
            <View style={styles.footerButtonLeft}>
            <Image
            source={require('../assets/return.png')}
            style={styles.footerImg}
            />
            </View>
          </TouchableOpacity>
          <View><Text>Connaître les zones d'une cuisine</Text></View>
          <TouchableOpacity>
            <View style={styles.footerButtonRight}>
              
            </View>
          </TouchableOpacity>
        </View>
        </ImageBackground>
      </View>
    );
  }

  const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      },
      image: {
        flex: 1,
        width: '100%',
        height: '100%',
        justifyContent: 'space-between'
      },footer: {
        width:'100%',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#f1f1f1',
        height:'6%',
        borderTopWidth: 1,
        borderColor: 'black',
        flexDirection:'row',
      },
      video: {
        height:'88%',
        width:'110%',
      },
      footerImg:{
        width:'35px',
        height:'35px',
      },
      footerButtonLeft:{
        width:'80px',
        height:'100%',
        justifyContent:'center',
        alignItems:'center',
        borderRightWidth:'1px',
        borderColor:'black',
      },
      footerButtonRight:{
        width:'80px',
        height:'100%',
        justifyContent:'center',
        alignItems:'center',
        borderLeftWidth:'1px',
        borderColor:'black',
      },
    
       buttonOn:{
         height: '75px',
         width: '30%',
         backgroundColor: 'white',
         opacity:'1',
         flexDirection:'row',
         alignItems:'center'
       },
       buttonOff:{
        height: '75px',
        width: '30%',
        backgroundColor: 'white',
        opacity:'0.4',
        flexDirection:'row',
        alignItems:'center'
       },
       row:{
         flexDirection: 'row',
         marginTop:'2%',
         justifyContent:'space-between'
       },
       icon:{
        width:'75px',
        height:'75px',
        backgroundColor:'darkred'
      },
      right:{
        width:'80%',
        justifyContent:'center',
        alignItems:'center',
      }
  });

  export default Kitchen2;