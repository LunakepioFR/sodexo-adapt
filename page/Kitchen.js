import * as React from 'react';
import { StyleSheet, Text, View, ImageBackground, TouchableOpacity, Image } from 'react-native';
import Header from '../components/header';
import { Video, AVPlaybackStatus} from 'expo-av';
function Kitchen({navigation}) {

    const onPressHandler = () => {
      navigation.navigate('Home');
    }
    const onSkip = () => {
      navigation.navigate('Kitchen2');
    }

    const video = React.useRef(null);
    const [status, setStatus] = React.useState({});


    return(
        <View style={styles.container}>
        <ImageBackground source={require('../assets/fond13.jpg')} style={styles.background}>
         <Header title="Découverte de la cuisine"/>
        <Video
        ref={video}
        style={styles.video}
        source={require('../assets/kitchenvid.mp4')}
        resizeMode='contain'
        shouldPlay
        onPlaybackStatusUpdate={status => setStatus(() => 'status') }
        />
        <View style={styles.footer}>
          <TouchableOpacity onPress={onPressHandler}>
            <View style={styles.footerButtonLeft}>
            <Image
            source={require('../assets/return.png')}
            style={styles.footerImg}
            />
            </View>
          </TouchableOpacity>
          <View><Text>Connaître les zones d'une cuisine</Text></View>
          <TouchableOpacity onPress={onSkip}>
            <View style={styles.footerButtonRight}>
              <Image
              source={require('../assets/right.png')}
              style={styles.footerImg}
              />
            </View>
          </TouchableOpacity>
        </View>
        </ImageBackground>
        </View>
    );
  }

  const styles = StyleSheet.create({
    container: {
        flex: 1,
      },
      background: {
        height: '100%',
        width:'100%',
        justifyContent:'space-between',
        alignItems:'center'
      },
      footer: {
        width:'100%',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#f1f1f1',
        height:'6%',
        borderTopWidth: 1,
        borderColor: 'black',
        flexDirection:'row',
      },
      video: {
        height:'88%',
        width:'110%',
      },
      footerImg:{
        width:'35px',
        height:'35px',
      },
      footerButtonLeft:{
        width:'80px',
        height:'100%',
        justifyContent:'center',
        alignItems:'center',
        borderRightWidth:'1px',
        borderColor:'black',
      },
      footerButtonRight:{
        width:'80px',
        height:'100%',
        justifyContent:'center',
        alignItems:'center',
        borderLeftWidth:'1px',
        borderColor:'black',
      }
      
     
  });

  export default Kitchen;