import React from 'react';
import { StyleSheet, Text, View, ImageBackground, Image, TouchableOpacity} from 'react-native';
import Header from '../components/header';


function Hot({ navigation }) {

    const onPressHandler = () => {
        navigation.navigate('Zone');
    }
  

    return(
      <View style={styles.container}>
        <Image source={require('../assets/fond7.jpg')} resizeMode="cover" style={styles.background}>
      
        
        </Image>
        <View style={styles.footer}>
          <TouchableOpacity onPress={onPressHandler}>
            <View style={styles.footerButtonLeft}>
            <Image
            source={require('../assets/return.png')}
            style={styles.footerImg}
            />
            </View>
          </TouchableOpacity>
          <View><Text>Hot</Text></View>
        </View>
      </View>
    )
  }
  const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    footer: {
      width:'100%',
      alignItems: 'center',
      justifyContent: 'space-between',
      backgroundColor: '#f1f1f1',
      height:'6%',
      borderTopWidth: 1,
      borderColor: 'black',
      flexDirection:'row',
    },
    background:{
        flex: 1,
        width:'100%',
        height:'100%'
    },
    buttonList: {
        flex:1,
        height:'100%', 

    },
    button: {
        backgroundColor:'white',
        borderBottomWidth:'1px',
        borderColor:'lightgrey',
        width:'20%',
        height:'8.35%',
        
    },
    right: {
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        height:'100%',
    },
    footerImg:{
      width:'35px',
      height:'35px',
    },

   
  });

  export default Hot;