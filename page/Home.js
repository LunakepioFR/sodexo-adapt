import React from 'react';
import { StyleSheet, Text, View, ImageBackground, TouchableOpacity } from 'react-native';
import Header from '../components/header';


function Homescreen({ navigation }) {

    const onPressHandler = () => {
      navigation.navigate('Kitchen');
  }
  
  const onSecondPress = () => {
    navigation.navigate('Zone');
  }
    return(
      <View style={styles.container}>
      <ImageBackground source={require('../assets/fond13.jpg')} style={styles.background}>
        <Header title="Découverte de la cuisine"/>

        <View style={styles.list}>
          <TouchableOpacity onPress={onPressHandler}>
            <View style={styles.buttonOn}>
              <Text>Connaître les zones d'une cuisine</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={onSecondPress}>
            <View style={styles.buttonOn}>
              <Text>Identifier les activités, le matériel, dans chaque zone</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View style={styles.buttonOff}>
              <Text>Mise en situation</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View style={styles.buttonOff}>
              <Text>Evaluation</Text>
            </View>
          </TouchableOpacity>


        </View>

        
        <View style={styles.footer}>
          <Text>Sommaire</Text>
        </View>
        </ImageBackground>
      </View>
    )
  }

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    footer: {
      width:'100%',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#f1f1f1',
      height:'6%',
      borderTopWidth: 1,
      borderColor: 'black'
    },
    background: {
      height: '100%',
      width:'100%',
      justifyContent:'space-between',
      alignItems:'center'
    },
    list:{
      width:'35%'
    },
    buttonOn:{
      backgroundColor:'white',
      alignItems:'center',
      justifyContent:'center',
      padding:'25px',
      borderColor:'grey',
      borderWidth:'1px',
    },
    buttonOff:{
      backgroundColor:'white',
      alignItems:'center',
      justifyContent:'center',
      padding:'25px',
      opacity:'0.4',
      borderColor:'grey',
      borderWidth:'1px',
    },


   
  });

  export default Homescreen;