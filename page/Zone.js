import React from 'react';
import { StyleSheet, Text, View, ImageBackground, TouchableOpacity, Image } from 'react-native';
import Header from '../components/header';


function Zone({ navigation }) {

  const onPress = () => {
    navigation.navigate('Home');
  }
  
  const onTest = () => {
    navigation.navigate('Vestiaire');
  }
  const Two = () => {
    navigation.navigate('Receipt');
  }
  const Three = () => {
    navigation.navigate('Stockage');
  }
  const Four = () => {
    navigation.navigate('UnderConditionning');
  }
  const Five = () => {
    navigation.navigate('Vegetable');
  }
  const Six = () => {
    navigation.navigate('Cold');
  }
  const Seven = () => {
    navigation.navigate('Hot');
  }
  const Eight = () => {
    navigation.navigate('Conditionning');
  }
  const Nine = () => {
    navigation.navigate('Distribution');
  }
  const Ten = () => {
    navigation.navigate('Dishes');
  }
  const Eleven = () => {
    navigation.navigate('Battery');
  }
  const Twelve = () => {
    navigation.navigate('Local');
  }
  
  

    return(
      <View style={styles.container}>
    <Header title="Découverte de la cuisine"/>
      <ImageBackground source={require('../assets/fond0.jpg')} resizeMode="stretch" style={styles.background}>
      <View style={styles.buttonList}>
        <TouchableOpacity onPress={onTest} style={styles.button}>
                <View style={styles.left}></View><View style={styles.right}><Text>Vestiaire</Text></View>
        </TouchableOpacity>
        <TouchableOpacity onPress={Two} style={styles.button}>
            <View style={styles.left}></View><View style={styles.right}><Text>Réception des marchandises</Text></View>
        </TouchableOpacity>
        <TouchableOpacity onPress={Three} style={styles.button}>
            <View style={styles.left}></View><View style={styles.right}><Text>Stockage</Text></View>
        </TouchableOpacity>
        <TouchableOpacity onPress={Four} style={styles.button}>
            <View style={styles.left}></View><View style={styles.right}><Text>Déconditionnement</Text></View>
        </TouchableOpacity>
        <TouchableOpacity onPress={Five} style={styles.button}>
            <View style={styles.left}></View><View style={styles.right}><Text>Légumerie</Text></View>
        </TouchableOpacity>
        <TouchableOpacity onPress={Six} style={styles.button}>
            <View style={styles.left}></View><View style={styles.right}><Text>Préparation froide</Text></View>
        </TouchableOpacity>
        <TouchableOpacity onPress={Seven} style={styles.button}>
            <View style={styles.left}></View><View style={styles.right}><Text>Préparation chaude</Text></View>
        </TouchableOpacity>
        <TouchableOpacity onPress={Eight} style={styles.button}>
            <View style={styles.left}></View><View style={styles.right}><Text>Conditionnement</Text></View>
        </TouchableOpacity>
        <TouchableOpacity onPress={Nine} style={styles.button}>
            <View style={styles.left}></View><View style={styles.right}><Text>Distribution, relation client, salle à manger</Text></View>
        </TouchableOpacity>
        <TouchableOpacity onPress={Ten} style={styles.button}>
            <View style={styles.left}></View><View style={styles.right}><Text>Plonge vaisselle</Text></View>
        </TouchableOpacity>
        <TouchableOpacity onPress={Eleven} style={styles.button}>
            <View style={styles.left}></View><View style={styles.right}><Text>Plonge batterie</Text></View>
        </TouchableOpacity>
        <TouchableOpacity onPress={Twelve} style={styles.button}>
            <View style={styles.left}></View><View style={styles.right}><Text>Local déchet</Text></View>
        </TouchableOpacity>

      </View>
        
        </ImageBackground>
        <View style={styles.footer}>
          <TouchableOpacity onPress={onPress}>
            <View style={styles.footerButtonLeft}>
            <Image
            source={require('../assets/return.png')}
            style={styles.footerImg}
            />
            </View>
          </TouchableOpacity>
          <View><Text>Connaître les zones d'une cuisine</Text></View>
        </View>
      </View>
    )
  }

const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    footer: {
      width:'100%',
      alignItems: 'center',
      justifyContent: 'space-between',
      backgroundColor: '#f1f1f1',
      height:'6%',
      borderTopWidth: 1,
      borderColor: 'black',
      flexDirection:'row',
    },
    background:{
        flex: 1,
        width:'100%',
        height:'100%'
    },
    buttonList: {
        flex:1,
        height:'100%', 

    },
    button: {
        backgroundColor:'white',
        borderBottomWidth:'1px',
        borderColor:'lightgrey',
        width:'20%',
        height:'8.35%',
        
    },
    right: {
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        height:'100%',
    },
    footerImg:{
      width:'35px',
      height:'35px',
    },

   
  });

  export default Zone;