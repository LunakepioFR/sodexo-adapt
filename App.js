import React from 'react';
import { StyleSheet, Text, View, ImageBackground, TouchableOpacity } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Homescreen from './page/Home';
import Kitchen2 from './page/Kitchen2';
import Kitchen from './page/Kitchen';
import Zone from './page/Zone';
import Vestiaire from './page/Vestiaire';
import Local from './page/Local';
import Battery from './page/Battery';
import Vegetable from './page/Vegetable';
import Cold from './page/Cold';
import Hot from './page/Hot';
import Stockage from './page/Stockage';
import UnderConditionning from './page/UnderConditionning';
import Dishes from './page/Dishes';
import Distribution from './page/Distribution';
import Receipt from './page/Receipt';
import Conditionning from './page/Conditionning';

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
      screenOptions={{
      headerShown: false
      }}
      >
        <Stack.Screen name="Home" component={Homescreen} />
        <Stack.Screen name="Kitchen" component={Kitchen}/>
        <Stack.Screen name="Kitchen2" component={Kitchen2}/>
      <Stack.Screen name="Zone" component={Zone}/>
      <Stack.Screen name="Vestiaire" component={Vestiaire}/>
      <Stack.Screen name="Local" component={Local}/>
      <Stack.Screen name="Battery" component={Battery}/>
      <Stack.Screen name="Vegetable" component={Vegetable}/>
      <Stack.Screen name="Cold" component={Cold}/>
      <Stack.Screen name="Hot" component={Hot}/>
      <Stack.Screen name="Stockage" component={Stockage}/>
      <Stack.Screen name="UnderConditionning" component={UnderConditionning}/>
      <Stack.Screen name="Dishes" component={Dishes}/>
      <Stack.Screen name="Distribution" component={Distribution}/>
      <Stack.Screen name="Receipt" component={Receipt}/>
      <Stack.Screen name="Conditionning" component={Conditionning}/>
      
      </Stack.Navigator>
    </NavigationContainer>
  );
}