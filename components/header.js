import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const Header = (props) => {
   return(
    <View style={styles.header}>
        <Text>{props.title}</Text>
    </View>
   )
};

export default Header;

const styles = StyleSheet.create({
    header:{
      width:'100%',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#f1f1f1',
      height:'6%',
      borderBottomWidth: 1,
      borderColor: 'black'
    }        
  });
